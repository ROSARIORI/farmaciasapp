package com.example.farmaciasapp

import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private val farmacias = mutableListOf<Farmacia>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        addFarmacia()
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun addFarmacia(){
        farmacias.add(Farmacia("Farmacia Colipa", 19.922566088269313, -96.72574983101337))
        farmacias.add(Farmacia("Farmacia San Francisco", 19.922396630765864, -96.72624758370759))
        farmacias.add(Farmacia("Farmacia Masegosa", 19.923373028395293, -96.7278867344826))

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val icon = getFarmaciaIcon()

        for (farmacia in farmacias){
            val farmaciaPosition = LatLng(farmacia.latitud,farmacia.longitud)
            val farmaciaName = farmacia.name

            val markerOptions = MarkerOptions().position(farmaciaPosition).title(farmaciaName)
                .icon(icon)
            mMap.addMarker(markerOptions)
        }

        // Add a marker in Sydney and move the camera
        val ayuntamiento = LatLng(19.922452108754584, -96.72516896674009)
        mMap.addMarker(MarkerOptions().position(ayuntamiento).title("Marker in Hayuntamiento"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ayuntamiento))
    }

    private fun getFarmaciaIcon(): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, R.drawable.ic_farmacia)
        drawable?.setBounds(0,0,drawable.intrinsicWidth,drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(drawable?.intrinsicWidth ?: 0,
            drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable?.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)


    }
}